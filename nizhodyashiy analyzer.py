
def g_input():
    print("Введите левые части правил строкой")
    Left_ = input()
    print("Введите правые части правил через пробел")
    Right_ = input()
    Right_ = Right_.split(' ')
    G = {}

    if len(Left_) != len(Right_):
        return None
    
    for l, r in zip(Left_, Right_):
        if G.get(l) is None:
            G.update( {l:[r]} )
        else:
            G[l].append(r)
    return G


class elemL1:
    def __init__(self, char, isTerminal, nAlt, countAlt):
        self.char = char
        self.isTerminal = isTerminal
        self.nAlt = nAlt
        self.countAlt = countAlt

    def print(self):
        print("("+str(self.char)+","+str(self.isTerminal)+","+str(self.nAlt)+","+str(self.countAlt)+")")
    def get_char(self):
        return self.char
    def get_isTerm(self):
        return self.isTerminal
    def get_nAlt(self):
        return self.nAlt
    def get_countAlt(self):
        return self.countAlt

class elemL2:
    def __init__(self, char, isTerminal):
        self.char = char
        self.isTerminal = isTerminal

    def print(self):
        print("("+str(self.char)+","+str(self.isTerminal)+")")
    def get_char(self):
        return self.char
    def get_isTerm(self):
        return self.isTerminal

def isTerm(char,T):
    try:
        T.index(char)
    except:
        return False
    return True

def print_stacks(L1, L2):
    print("L1")
    for nam in L1:
        nam.print()
    print("L2")
    for nam in L2:
        nam.print()
    print("-----------")

def h(L1, rules):
    otvet = []
    new_dict = {}
    count = 1
    for line in rules.keys():
        for rule in rules.get(line):
            new_dict.update( {rule : count} )
            count += 1

    for elem in L1:
        if elem.get_isTerm() == True:
            continue
        else:
            otvet.append(new_dict[rules[elem.get_char()][elem.get_nAlt()]])
    return otvet
            
def G_1(P, terminals, input_str, start_neterminal):
    state = 'q'
    i = 0
    len_str = len(input_str)
    L2 = [elemL2(start_neterminal, False)]
    L1 = []
    while True:
        if state == 'q':
            elem = L2[-1]
            if elem.get_isTerm() == True:
                if elem.get_char() == input_str[i]:
                #шаг 2: успешное сравнение терминального символа с символом вх цепочки:  
                    L2.pop()
                    L1.append( elemL1(elem.get_char(), elem.get_isTerm(), None, None) )
                    i += 1
                    if i == len_str:
                        if len(L2) == 0:#шаг 3 - успешное завершение
                            state = 't'
                            continue
                        else:#шаг 3': неуспешное завершение, L2 не пуст(цепочка прочитана, но есть нерасмотренные вершины)
                            state = 'b'
                            continue
                    else:
                        if len(L2) == 0:#шаг 3': неуспешное завершение L2 пуст (в дереве уже нет вершин, а цепочка прочитана не вся)
                            state = 'b'
                            continue
                        else:  
                            continue
                else:
                    #шаг 4: текущий терминал не совмал с символом вх цепочки
                    state = 'b'
                    continue
            else:#шаг 1: разрастание дерева - применяем первую альтернативу к вершине elem: 
                L2.pop()
                L1.append( elemL1(elem.get_char(), elem.get_isTerm(), 0, len(P[elem.get_char()])) )
                str_to_append = P[elem.get_char()][0]
                str_to_append = str_to_append[::-1]
                for char in str_to_append:
                    L2.append( elemL2(char, isTerm(char, terminals)) )

                continue
        elif state == 'b':
            elem = L1[-1]
            if elem.get_isTerm() == True:#шаг 5: возврат по терминалу
                L1.pop()
                L2.append( elemL2(elem.get_char(), elem.get_isTerm()) )#перенос из L1 в L2
                i -= 1
                continue

            else:
                #шаг 6 - испытание новой альтернативы
                if elem.get_nAlt() < elem.get_countAlt()-1:#если перебрали ещё не все альтернативы
                    #шаг 6а: у нетерминала elem есть ещё альтернативы
                    L1.pop()# меняем индекс в L1 elem(j) на elem(j+1)
                    new_el_to_add = elemL1(elem.get_char(), elem.get_isTerm(), elem.get_nAlt()+1, elem.get_countAlt())
                    L1.append(new_el_to_add)

                    #удаляем предыдущую альтернативу
                    num_alt_to_del = elem.get_nAlt()
                    for j in P[elem.get_char()][num_alt_to_del]:
                        L2.pop()
                    
                    #добавляем новую альтернативу
                    str_to_append = P[elem.get_char()][num_alt_to_del+1]
                    str_to_append = str_to_append[::-1]
                    for char in str_to_append:
                        L2.append( elemL2(char, isTerm(char, terminals)) )
                    
                    state = 'q'
                    continue
                else:
                    if elem.get_char() == start_neterminal and i == 0:
                        #шаг 6б следующая альтернатива невозможна, так как мы находимся в корне дерева
                        return -1
                    else:
                        #6в: текущая вершина != корню дерева => возврат на более высокий уровень дерева
                        #удаляем правую часть в L2
                        for j in P[elem.get_char()][elem.get_nAlt()]:
                            L2.pop()
                        #заменяем на левую
                        L2.append( elemL2(elem.get_char(), False))
                        #удаляем альтернативу elem из L1
                        L1.pop()

                        continue
        elif state == 't':
            return h(L1, P)

G = {'B': ["T+B", "T"], 'T': ["M", "M*T"], 'M': ["a", "b"]}
T = "!+*ab()"
S = 'B'
input_str = "a+b"
#G = g_input()
if G == None:
    print("Ошибка при вводе")
else:
    otvet = G_1(G, T, input_str, S)
    if otvet == -1:
        print("Ошибка")
    else:
        print(otvet)



