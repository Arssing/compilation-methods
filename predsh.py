def equal(gram):#Отношения, стоящие рядом
    equal_ = []
    for Neterm in gram:
        for rule in gram[Neterm]:
            if len(rule) > 1:
                for j in range(len(rule)-1):
                    equal_.append(rule[j]+rule[j+1])
    return equal_
def less(gram, T):#терминал-нетеминал
    less_ = []
    for Neterm in gram:
        for rule in gram[Neterm]:
            if len(rule) > 1:
                for j in range(len(rule)-1):
                    if (rule[j] in T) and (rule[j+1] not in T):
                        less_.append(rule[j]+rule[j+1])
    return less_
def more(gram, T):#нетеминал-теминал или нетерминал-нетерминал
    more_ = []
    for Neterm in gram:
        for rule in gram[Neterm]:
            if len(rule) > 1:
                for j in range(len(rule)-1):
                    if ((rule[j] not in T) and (rule[j+1] in T)) or ((rule[j] not in T) and (rule[j+1] not in T)):
                        more_.append(rule[j]+rule[j+1])
    return more_
def print_matrix(matrix):
    for line in matrix:
        print("\t", end='')
        for char in line:
            print(str(char)+"    ", end='')
        print("\n")

def get_matrix(gram,L,R, equal_rule, less_rule, more_rule):
    #находим все символы в наше грамматике
    all_symb = [] 
    for Neterm in gram:
        for rule in gram[Neterm]:
            for char in rule:
                if char not in all_symb:
                    all_symb.append(char)

    matrix = []
    #делаем матрицу более наглядной
    all_sym = []
    all_sym.append(" ")
    for s in all_symb:
        all_sym.append(s)
    matrix.append(all_sym)
    
    for char in all_symb:
        new_row = [char]
        for char2 in all_symb:
            new_row.append("-")
        matrix.append(new_row)
    
    #словарь для преобразования из символа в индекс
    from_eq_rule_to_int = {}
    count = 1
    for char in all_symb:
        from_eq_rule_to_int.update({char:count})
        count+=1
    
    for l in equal_rule:
        i = from_eq_rule_to_int[l[0]]
        j = from_eq_rule_to_int[l[1]]
        matrix[i][j] = "="
    
    for l in less_rule:
        #первый символ - терминал
        i = from_eq_rule_to_int[l[0]]
        #второй символ - нетерминал (например, B, ставим знак < для всего множества L(B))
        char = l[1]
        for sym in L[char]:
            #заполняем по строкам
            j = from_eq_rule_to_int[sym]
            if matrix[i][j] == "-":
                matrix[i][j] = "<"
            else:
                matrix[i][j] += "<"
    
    
    for l in more_rule:
        #первый символ - нетерминал
        char = l[0]
        #второй символ - терминал или нетерминал
        i = from_eq_rule_to_int[l[1]]
        for sym in R[char]:
            #заполняем по столбцам
            j = from_eq_rule_to_int[sym]
            if matrix[j][i] == "-":
                matrix[j][i] = ">"
            else:
                matrix[j][i] += ">"
    
    return matrix, all_symb



def get_L_R(G1):
    L = {}
    R = {}
    #шаг 1
    #идем по нетерминалам правил, которые находятся слева от правил
    for char in G1:
        #создаем список левых и правых правил для нетерминала
        L[char] = []
        R[char] = []
        #идем по каждой альтернативе
        for rule in G1[char]:
            #1. Включаем самые левые символы правил для нетерминала char
            if rule[0] not in L[char]:
                L[char].append(rule[0])
            #1. Включаем самые правые символы правил для нетерминала char
            if rule[-1] not in R[char]:
                R[char].append(rule[-1])

    #шаг 2
    for char1, char2 in zip(L,R):
        # просматриваем уже построенное мн-во L(U), если встретили нетерминал T и у него
        # в его мн-ве L(T) есть то, чего нет в L(U), то мы эти символы добавляем рядом
        for l in L[char1]:
            if l in L.keys():
                for sym in L[l]:
                    if sym not in L[char1]:
                        L[char1].append(sym)
        # просматриваем уже построенное мн-во R(U), если встретили нетерминал T и у него
        # в его мн-ве R(T) есть то, чего нет в R(U), то мы эти символы добавляем рядом
        for r in R[char2]:
            if r in R.keys():
                for sym in R[r]:
                    if sym not in R[char2]:
                        R[char2].append(sym)
    return L,R

def print_gram(Gramm):
    count = 1
    for char in Gramm:
        print(char+" -> ",end='')
        for rule in Gramm[char]:
            print(rule+" ["+str(count)+"]"+" | ",end='')
            count += 1
        print()

def get_all_neterm(Gramm):
    all_neterminals = ""
    for char in Gramm:
        all_neterminals += char
    return all_neterminals

def del_neterm_from_str(string, neterm_to_del):
    for char in neterm_to_del:
        if char in string:
            new_str = string.replace(char, "")
            string = new_str
    return string

def stratification(Gramm, L, R):
    neterminals = "QWERTYUIOPASDFGHJKLZXCVBNM"
    neterm = get_all_neterm(Gramm)
    neterminals = del_neterm_from_str(neterminals, neterm)
    new_Gramm = {}
    for char in Gramm:
        flag = 0
        #если правило имеет вид B->B...
        if char in L[char]:
            flag = 1
            #добавляем новый нетерминальный символ и правило вида B->W
            new_char = neterminals[0]
            neterminals = del_neterm_from_str(neterminals, new_char)
            new_Gramm.update({char:[new_char]})
            #заменяем все алтернативы для B->... => W->...
            new_right_side = []
            for i in range(len(Gramm[char])):
                new_rule = ""
                for char_ in Gramm[char][i]:
                    if char_ == char:
                        new_rule += new_char
                    else:
                        new_rule += char_
                new_right_side.append(new_rule)
            new_Gramm.update({new_char:new_right_side})
        
        if char in R[char]:
            flag = 1
            new_char = neterminals[0]
            neterminals = del_neterm_from_str(neterminals, new_char)
            new_Gramm.update({char:[new_char]})
            new_right_side = []
            for i in range(len(Gramm[char])):
                new_rule = ""
                for char_ in Gramm[char][i]:
                    if char_ == char:
                        new_rule += new_char
                    else:
                        new_rule += char_
                new_right_side.append(new_rule)
            new_Gramm.update({new_char:new_right_side})
        
        if flag == 0:
            new_Gramm.update({char:Gramm[char]})
    return new_Gramm

def get_start_symb(Gramm):
    for char in Gramm:
        return char

def get_rule_by_right_side(gram, right_side):
    #получаем номер правила и его левую часть по правой части
    count = 1
    for left_side in gram:
        for rule in gram[left_side]:
            if rule == right_side:
                return count, left_side
            count += 1
    return -1

def Pred(Gramm, Term, input_str):
    #начальные вычисления
    otvet = []
    L, R = get_L_R(Gramm)
    new_Gramm = stratification(Gramm, L, R)

    print("----------")
    print_gram(Gramm)
    print("----------")
    print_gram(new_Gramm)
    print("----------")
    

    L, R = get_L_R(new_Gramm)
    equal_rule = equal(new_Gramm)
    less_rule = less(new_Gramm,Term)
    more_rule = more(new_Gramm,Term)
    matrix, all_symb = get_matrix(new_Gramm, L, R, equal_rule, less_rule, more_rule)

    #print_matrix(matrix)

    index = {}
    count = 1
    for char in all_symb:
        index.update({char:count})
        count+=1
    start_symb = get_start_symb(new_Gramm)


    stack = []
    stack.append(input_str[0])
    i = 0
    flag = 0
    while stack[-1] != start_symb:
        if i < len(input_str)-1:
            i+=1
            input_char = input_str[i]
        else:
            flag = 1
        compare = matrix[index[stack[-1]]][index[input_char]]
        
        #print(str(stack)+" "+stack[-1]+compare+input_char)

        if compare == '>' or flag == 1:
            find_rule = ""

            while compare != '<' and stack:
                fr = stack.pop()
                find_rule = fr + find_rule
                if not stack:
                    continue
                else:
                    st = stack[-1]
                    compare = matrix[index[st]][index[fr]]
                    
                    #print(st+compare+fr)
                    if compare == '-':
                        print("Error")
                        return -1
                
            num_rule, left_side = get_rule_by_right_side(new_Gramm, find_rule)
            print(find_rule+"->"+left_side+"\t["+str(num_rule)+"]")
            otvet.append(num_rule)
            stack.append(left_side)
            i -= 1
            
        elif compare == '-':
            print("Error")
            return -1
        else:
            stack.append(input_char)
    
    return otvet


G1 = {'A':["!B!"], 'B':["B+T", "T"], 'T':["T*M", "M"],'M':['a','b','c',"(B)"]}
T = "!+*ab()"
input_str = "!(a+b)c!"
print(Pred(G1, T, input_str))